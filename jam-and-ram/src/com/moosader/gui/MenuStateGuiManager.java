package com.moosader.gui;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.moosader.jamram.GlobalOptions;

public class MenuStateGuiManager {
	private Vector2 m_screenInitialDimen;

	// Da Textures
	private Texture m_txTempMenuBg;
	private Texture m_txHumanAiSelectionButtons;
	
	private BitmapFont m_font;
	private CharSequence m_player1Key;
	private CharSequence m_player2Key;
	
	ArrayList<GuiElement> m_lstElements;

	public void setup() {
		m_screenInitialDimen = new Vector2(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		if ( GlobalOptions.halloweenVersion() ) {
			m_txTempMenuBg = new Texture(Gdx.files.internal("data/halloween/gfx/menus/MenuBase.png"));
		}
		else {
			m_txTempMenuBg = new Texture(Gdx.files.internal("data/gfx/menus/MenuBase.png"));			
		}
		m_txTempMenuBg.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		
		m_txHumanAiSelectionButtons = new Texture(Gdx.files.internal("data/gfx/menus/ButtonsHumanAI.png"));
		m_txHumanAiSelectionButtons.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		
		m_lstElements = new ArrayList<GuiElement>();
		
		m_font = new BitmapFont(Gdx.files.internal("data/fonts/Averia.fnt"),
				Gdx.files.internal("data/fonts/Averia.png"), false);

		
		GuiElement bg = new GuiElement("menubackground", m_txTempMenuBg);
		bg.setDimensions(new Vector2(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
		bg.setImageRegion(new Vector2(0, 0), new Vector2(1000,1000));
		bg.setPosition(new Vector2(0, 0));	
		m_lstElements.add(bg);

		if ( !GlobalOptions.halloweenVersion() ) {
			setupHumanAIButtons();
		}
	}
	
	public void addElementsToSpriteList(ArrayList<Sprite> lstSprites) {
		for (GuiElement g : m_lstElements) {
			lstSprites.add(g.getSprite());
		}
	}
	
	public void drawText(SpriteBatch batch) {		
		/*
		m_font.setColor( 1.0f, 1.0f, 1.0f, 1.0f );
		
		float xPos = m_screenInitialDimen.x / 5;
		float yPos = 172;
		
		m_font.draw(batch, m_player1Key, 84, yPos);
		m_font.draw(batch, m_player2Key, 298, yPos);
		*/		
	}
	
	// TODO: Allow key changing
	public void setPlayer1Key() {
		
	}
	
	public void setPlayer2Key() {
		
	}
	
	private void setupHumanAIButtons() {
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		
		float newWidth = 0.05f * w;
		float newHeight = 0.08f * h;
		
		GuiElement btnP1Human = new GuiElement("p1human", m_txHumanAiSelectionButtons);
		btnP1Human.setDimensions(new Vector2(newWidth, newHeight));
		btnP1Human.setImageRegion(new Vector2(100, 160), new Vector2(100, 160));
		btnP1Human.setPosition(new Vector2(0.352f * w, 0.911f * h - newHeight));
		m_lstElements.add(btnP1Human);
		
		GuiElement btnP1AI = new GuiElement("p1ai", m_txHumanAiSelectionButtons);
		btnP1AI.setDimensions(new Vector2(newWidth, newHeight));
		btnP1AI.setImageRegion(new Vector2(0, 0), new Vector2(100, 160));
		btnP1AI.setPosition(new Vector2(0.415f * w, 0.911f * h - newHeight));
		m_lstElements.add(btnP1AI);
		
		GuiElement btnP2Human = new GuiElement("p2human", m_txHumanAiSelectionButtons);
		btnP2Human.setDimensions(new Vector2(newWidth, newHeight));
		btnP2Human.setImageRegion(new Vector2(0, 160), new Vector2(100, 160));
		btnP2Human.setPosition(new Vector2(0.856f * w, 0.911f * h - newHeight));
		m_lstElements.add(btnP2Human);
		
		GuiElement btnP2AI = new GuiElement("p2ai", m_txHumanAiSelectionButtons);
		btnP2AI.setDimensions(new Vector2(newWidth, newHeight));
		btnP2AI.setImageRegion(new Vector2(100, 0), new Vector2(100, 160));
		btnP2AI.setPosition(new Vector2(0.919f * w, 0.911f * h - newHeight));
		m_lstElements.add(btnP2AI);
		
		
		m_player1Key = "A";
		m_player2Key = "L";
	}
}
