package com.moosader.gui;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.moosader.entities.EntityBase;

public class GuiElement extends EntityBase {
	private int m_state;
	private String m_name;
	
	public GuiElement(String name, Texture texture) {
		m_coord = new Vector2();
		m_dimen = new Vector2();
		m_region = new TextureRegion(texture);
		m_sprite = new Sprite();
		m_name = new String(name);
	}
	
	public Sprite getSprite() {
		return m_sprite;
	}
	
	public void setTexture(Texture texture) {
		m_region.setRegion(texture);
		updateFrame();
	}
	
	public void setPosition(Vector2 coord) {
		m_coord = coord;		
		updateFrame();
	}
	
	public void setDimensions(Vector2 dimen) {
		m_dimen = dimen;		
		updateFrame();
	}
	
	public void setImageRegion(Vector2 txCoord, Vector2 txDimen) {
		m_region.setRegionX((int) txCoord.x);
		m_region.setRegionY((int) txCoord.y);
		m_region.setRegionWidth((int) txDimen.x);
		m_region.setRegionHeight((int) txDimen.y);
		updateFrame();
	}
	
	public void updateFrame() {
		// Update texture region and sprite
		m_sprite.setSize(m_dimen.x, m_dimen.y);
		m_sprite.setPosition(m_coord.x, m_coord.y);
		m_sprite.setRegion(m_region);
	}
}
