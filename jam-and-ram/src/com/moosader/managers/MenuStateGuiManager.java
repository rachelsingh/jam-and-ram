package com.moosader.managers;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.moosader.gui.GuiElement;

public class MenuStateGuiManager {	
	/*
	private BitmapFont m_font;
	private CharSequence m_player1Key;
	private CharSequence m_player2Key;
	*/
	
	ArrayList<GuiElement> m_lstElements;

	public void setup() {		
		m_lstElements = new ArrayList<GuiElement>();
		
		//m_font = new BitmapFont(Gdx.files.internal("data/fonts/Averia.fnt"), Gdx.files.internal("data/fonts/Averia.png"), false);

		GuiElement bg = new GuiElement("menubackground", GraphicsManager.txMenuBackground.texture);
		bg.setDimensions(new Vector2(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
		bg.setImageRegion(new Vector2(0, 0), GraphicsManager.txMenuBackground.frameDimen);
		bg.setPosition(new Vector2(0, 0));	
		m_lstElements.add(bg);
	}
	
	public void addElementsToSpriteList(ArrayList<Sprite> lstSprites) {
		for (GuiElement g : m_lstElements) {
			lstSprites.add(g.getSprite());
		}
	}
	
	public void drawText(SpriteBatch batch) {		
		/*
		m_font.setColor( 1.0f, 1.0f, 1.0f, 1.0f );
		
		float xPos = m_screenInitialDimen.x / 5;
		float yPos = 172;
		
		m_font.draw(batch, m_player1Key, 84, yPos);
		m_font.draw(batch, m_player2Key, 298, yPos);
		*/		
	}
	
	// TODO: Allow key changing
	public void setPlayer1Key() {
		
	}
	
	public void setPlayer2Key() {
		
	}
}
