package com.moosader.managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.math.Vector2;
import com.moosader.jamram.GlobalOptions;

public class GraphicsManager {
	public static class TextureWrapper {
		public Vector2 frameDimen;
		public Texture texture;
		public String tempPath;
		
		public int frameW() {
			return (int)(frameDimen.x);
		}
		
		public int frameH() {
			return (int)(frameDimen.y);
		}
		
		public TextureWrapper() {
			frameDimen = new Vector2();
		}

		public void setFrameDimen(Vector2 dimen) {
			frameDimen = dimen;
		}
		
		public void loadTexture(String path) {
			tempPath = path;
			texture = new Texture(Gdx.files.internal(path));	
			texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		}
	};

	private static final String halloweenSDPath = "data/gfx/SD/HF/";
	private static final String halloweenHDPath = "data/gfx/HD/HF/";
	private static final String originalSDPath = "data/gfx/SD/JAR/";
	private static final String originalHDPath = "data/gfx/HD/JAR/";
	
	public static String graphicsPath;
	private static boolean menuGraphicsLoaded = false;
	private static boolean gameGraphicsLoaded = false;
	
	public static TextureWrapper txPlayer;
	public static TextureWrapper txPlayerBullet;
	public static TextureWrapper txBoss;

	public static TextureWrapper txEnemy1;
	public static TextureWrapper txEnemy2;
	public static TextureWrapper txEnemy3;
	public static TextureWrapper txEnemy4;
	public static TextureWrapper txEnemy5;
	public static TextureWrapper txEnemy6;
	public static TextureWrapper txEnemyBullet;
	
	public static TextureWrapper txEffectExplosion;
	public static TextureWrapper txHudHP;

	public static TextureWrapper txMenuTitlescreen1;
	public static TextureWrapper txMenuTitlescreen2;
	public static TextureWrapper txMenuBackground;
	public static TextureWrapper txIntroSlide;
	public static TextureWrapper txEndingSlide;

	public static TextureWrapper txLevel1Sky;
	public static TextureWrapper txLevel1Background1;
	public static TextureWrapper txLevel1Background2;
	public static TextureWrapper txLevel1Background3;
	public static TextureWrapper txLevel1Foreground;

	public static TextureWrapper txLevel2Sky;
	public static TextureWrapper txLevel2Background1;
	public static TextureWrapper txLevel2Background2;
	public static TextureWrapper txLevel2Background3;
	public static TextureWrapper txLevel2Foreground;
	
	public GraphicsManager() {
	}

	public static void setup() {
		initializeShit();
		
		if (GlobalOptions.isAndroid()) {
			System.out.println( "Android/SD Platform" );
			if (GlobalOptions.isHalloween()) {
				System.out.println( "Halloween Version" );
				graphicsPath = halloweenSDPath;
				setupHallowenSDDimensions();
			}
			else {
				System.out.println( "JAM&RAM Version" );
				graphicsPath = originalSDPath;
				setupOriginalSDDimensions();
			}
		}
		else {
			System.out.println( "PC/HD Platform" );
			if (GlobalOptions.isHalloween()) {
				System.out.println( "Halloween Version" );
				graphicsPath = halloweenHDPath;
				setupHallowenHDDimensions();
			}
			else {
				System.out.println( "JAM&RAM Version" );
				graphicsPath = originalHDPath;
				setupOriginalHDDimensions();
			}
		}
	}
	
	public static void initializeShit() {
		 txPlayer = new TextureWrapper();
		 txPlayerBullet = new TextureWrapper();
		 txBoss = new TextureWrapper();

		 txEnemy1 = new TextureWrapper();
		 txEnemy2 = new TextureWrapper();
		 txEnemy3 = new TextureWrapper();
		 txEnemy4 = new TextureWrapper();
		 txEnemy5 = new TextureWrapper();
		 txEnemy6 = new TextureWrapper();
		 txEnemyBullet = new TextureWrapper();
		
		 txEffectExplosion = new TextureWrapper();
		 txHudHP = new TextureWrapper();

		 txMenuTitlescreen1 = new TextureWrapper();
		 txMenuTitlescreen2 = new TextureWrapper();
		 txMenuBackground = new TextureWrapper();
		 txIntroSlide = new TextureWrapper();
		 txEndingSlide = new TextureWrapper();

		 txLevel1Sky = new TextureWrapper();
		 txLevel1Background1 = new TextureWrapper();
		 txLevel1Background2 = new TextureWrapper();
		 txLevel1Background3 = new TextureWrapper();
		 txLevel1Foreground = new TextureWrapper();

		 txLevel2Sky = new TextureWrapper();
		 txLevel2Background1 = new TextureWrapper();
		 txLevel2Background2 = new TextureWrapper();
		 txLevel2Background3 = new TextureWrapper();
		 txLevel2Foreground = new TextureWrapper();
	}
	
	public static void loadMenuImages() {
		if (!menuGraphicsLoaded) {
			txIntroSlide.loadTexture(graphicsPath + "menus/IntroDraft.png");
			txEndingSlide.loadTexture(graphicsPath + "menus/EndingDraft.png");
			txMenuTitlescreen1.loadTexture(graphicsPath + "menus/TitleImageYellow.png");
			txMenuTitlescreen2.loadTexture(graphicsPath + "menus/TitleImageRed.png");
			txMenuBackground.loadTexture(graphicsPath + "menus/MenuBase.png");
			
			menuGraphicsLoaded = true;
		}
	}
	
	public static void loadGameplayImages() {
		if (!gameGraphicsLoaded) {
			txPlayer.loadTexture(graphicsPath + "entities/Character_Helicopter.png");
			txPlayerBullet.loadTexture(graphicsPath + "entities/PlayerBullet.png");
			txHudHP.loadTexture(graphicsPath + "effects/GUI_Heart.png");
			
			txEnemy1.loadTexture(graphicsPath + "entities/Character_DualAgents1.png");
			txEnemy2.loadTexture(graphicsPath + "entities/Character_DualAgents2.png");
			txEnemy3.loadTexture(graphicsPath + "entities/Character_PistolAgent1.png");
			txEnemy4.loadTexture(graphicsPath + "entities/Character_PistolAgent2.png");
			txEnemy5.loadTexture(graphicsPath + "entities/Character_ShotgunAgent1.png");
			txEnemy6.loadTexture(graphicsPath + "entities/Character_ShotgunAgent2.png");
			txBoss.loadTexture(graphicsPath + "entities/Character_Jad.png");	
			
			txEnemyBullet.loadTexture(graphicsPath + "entities/EnemyBullet.png");
			txEffectExplosion.loadTexture(graphicsPath + "effects/Effect_Explosion.png");
			
			txLevel1Sky.loadTexture(graphicsPath + "environment/Scenery_Sky_Day.png");
			txLevel1Background1.loadTexture(graphicsPath + "environment/Scenery_Buildings1_Day.png");
			txLevel1Background2.loadTexture(graphicsPath + "environment/Scenery_Buildings2_Day.png");
			txLevel1Background3.loadTexture(graphicsPath + "environment/Scenery_Buildings3_Day.png");
			txLevel1Foreground.loadTexture(graphicsPath + "environment/Scenery_BuildingsF_Day.png");
	
			txLevel2Sky.loadTexture(graphicsPath + "environment/Scenery_Sky_Night.png");
			txLevel2Background1.loadTexture(graphicsPath + "environment/Scenery_Buildings1_Night.png");
			txLevel2Background2.loadTexture(graphicsPath + "environment/Scenery_Buildings2_Night.png");
			txLevel2Background3.loadTexture(graphicsPath + "environment/Scenery_Buildings3_Night.png");
			txLevel2Foreground.loadTexture(graphicsPath + "environment/Scenery_BuildingsF_Night.png");
		}
	}
	
	private static void setupOriginalHDDimensions() {
		// Set dimensions
		txPlayer.setFrameDimen( new Vector2(342, 217));
		txPlayerBullet.setFrameDimen( new Vector2(32, 32));
		txHudHP.setFrameDimen( new Vector2(32, 32));

		txEnemy1.setFrameDimen( new Vector2(488, 508));
		txEnemy2.setFrameDimen( new Vector2(488, 508));
		txEnemy3.setFrameDimen( new Vector2(500, 250));
		txEnemy4.setFrameDimen( new Vector2(500, 250));
		txEnemy5.setFrameDimen( new Vector2(250, 600));
		txEnemy6.setFrameDimen( new Vector2(250, 600));
		
		txEnemyBullet.setFrameDimen( new Vector2(32, 32));
		txEffectExplosion.setFrameDimen( new Vector2(128,128));
		
		txBoss.setFrameDimen( new Vector2(309,177));

		txIntroSlide.setFrameDimen( new Vector2(1000, 1000));
		txEndingSlide.setFrameDimen( new Vector2(1000, 1000));
		txMenuTitlescreen1.setFrameDimen( new Vector2(480, 480));
		txMenuTitlescreen2.setFrameDimen( new Vector2(480, 480));
		txMenuBackground.setFrameDimen( new Vector2(1000, 1000));

		txLevel1Sky.setFrameDimen( new Vector2(500, 500));
		txLevel1Background1.setFrameDimen( new Vector2(1500, 500));
		txLevel1Background2.setFrameDimen( new Vector2(1500, 500));
		txLevel1Background3.setFrameDimen( new Vector2(1500, 500));
		txLevel1Foreground.setFrameDimen(  new Vector2(1500, 500));

		txLevel2Sky.setFrameDimen( new Vector2(500, 500));
		txLevel2Background1.setFrameDimen( new Vector2(1500, 500));
		txLevel2Background2.setFrameDimen( new Vector2(1500, 500));
		txLevel2Background3.setFrameDimen( new Vector2(1500, 500));
		txLevel2Foreground.setFrameDimen( new Vector2(1500, 500));
	}
	
	private static void setupHallowenHDDimensions() {
		// Set dimensions
		txPlayer.setFrameDimen( new Vector2(300, 256));
		txPlayerBullet.setFrameDimen( new Vector2(32, 32));
		txHudHP.setFrameDimen( new Vector2(32, 32));

		txEnemy1.setFrameDimen( new Vector2(470, 426));
		txEnemy2.setFrameDimen( new Vector2(470, 426));
		txEnemy3.setFrameDimen( new Vector2(511, 378));
		txEnemy4.setFrameDimen( new Vector2(511, 378));
		txEnemy5.setFrameDimen( new Vector2(500, 493));
		txEnemy6.setFrameDimen( new Vector2(500, 493));
		
		txEnemyBullet.setFrameDimen( new Vector2(32, 32));
		txEffectExplosion.setFrameDimen( new Vector2(128,128));
		
		txBoss.setFrameDimen( new Vector2(309,177));

		txIntroSlide.setFrameDimen( new Vector2(1000, 1000));
		txEndingSlide.setFrameDimen( new Vector2(1000, 1000));
		txMenuTitlescreen1.setFrameDimen( new Vector2(1000, 1000));
		txMenuTitlescreen2.setFrameDimen( new Vector2(1000, 1000));
		txMenuBackground.setFrameDimen( new Vector2(1000, 1000));

		txLevel1Sky.setFrameDimen( new Vector2(500, 500));
		txLevel1Background1.setFrameDimen( new Vector2(1500, 500));
		txLevel1Background2.setFrameDimen( new Vector2(1500, 500));
		txLevel1Background3.setFrameDimen( new Vector2(1500, 500));
		txLevel1Foreground.setFrameDimen(  new Vector2(1500, 500));

		txLevel2Sky.setFrameDimen( new Vector2(500, 500));
		txLevel2Background1.setFrameDimen( new Vector2(1500, 500));
		txLevel2Background2.setFrameDimen( new Vector2(1500, 500));
		txLevel2Background3.setFrameDimen( new Vector2(1500, 500));
		txLevel2Foreground.setFrameDimen( new Vector2(1500, 500));
	}
	
	private static void setupOriginalSDDimensions() {
		// Set dimensions
		txPlayer.setFrameDimen( new Vector2(300, 256));
		txPlayerBullet.setFrameDimen( new Vector2(32, 32));
		txHudHP.setFrameDimen( new Vector2(32, 32));

		txEnemy1.setFrameDimen( new Vector2(470, 426));
		txEnemy2.setFrameDimen( new Vector2(470, 426));
		txEnemy3.setFrameDimen( new Vector2(511, 378));
		txEnemy4.setFrameDimen( new Vector2(511, 378));
		txEnemy5.setFrameDimen( new Vector2(500, 493));
		txEnemy6.setFrameDimen( new Vector2(500, 493));
		
		txEnemyBullet.setFrameDimen( new Vector2(32, 32));
		txEffectExplosion.setFrameDimen( new Vector2(128,128));
		
		txBoss.setFrameDimen( new Vector2(309,177));

		txIntroSlide.setFrameDimen( new Vector2(1000, 1000));
		txEndingSlide.setFrameDimen( new Vector2(1000, 1000));
		txMenuTitlescreen1.setFrameDimen( new Vector2(1000, 1000));
		txMenuTitlescreen2.setFrameDimen( new Vector2(1000, 1000));
		txMenuBackground.setFrameDimen( new Vector2(1000, 1000));

		txLevel1Sky.setFrameDimen( new Vector2(500, 500));
		txLevel1Background1.setFrameDimen( new Vector2(1500, 500));
		txLevel1Background2.setFrameDimen( new Vector2(1500, 500));
		txLevel1Background3.setFrameDimen( new Vector2(1500, 500));
		txLevel1Foreground.setFrameDimen(  new Vector2(1500, 500));

		txLevel2Sky.setFrameDimen( new Vector2(500, 500));
		txLevel2Background1.setFrameDimen( new Vector2(1500, 500));
		txLevel2Background2.setFrameDimen( new Vector2(1500, 500));
		txLevel2Background3.setFrameDimen( new Vector2(1500, 500));
		txLevel2Foreground.setFrameDimen( new Vector2(1500, 500));
		
	}
	
	private static void setupHallowenSDDimensions() {
		// Set dimensions
		txPlayer.setFrameDimen( new Vector2(300, 256));
		txPlayerBullet.setFrameDimen( new Vector2(32, 32));
		txHudHP.setFrameDimen( new Vector2(32, 32));

		txEnemy1.setFrameDimen( new Vector2(470, 426));
		txEnemy2.setFrameDimen( new Vector2(470, 426));
		txEnemy3.setFrameDimen( new Vector2(511, 378));
		txEnemy4.setFrameDimen( new Vector2(511, 378));
		txEnemy5.setFrameDimen( new Vector2(500, 493));
		txEnemy6.setFrameDimen( new Vector2(500, 493));
		
		txEnemyBullet.setFrameDimen( new Vector2(32, 32));
		txEffectExplosion.setFrameDimen( new Vector2(128,128));
		
		txBoss.setFrameDimen( new Vector2(309,177));

		txIntroSlide.setFrameDimen( new Vector2(1000, 1000));
		txEndingSlide.setFrameDimen( new Vector2(1000, 1000));
		txMenuTitlescreen1.setFrameDimen( new Vector2(1000, 1000));
		txMenuTitlescreen2.setFrameDimen( new Vector2(1000, 1000));
		txMenuBackground.setFrameDimen( new Vector2(1000, 1000));

		txLevel1Sky.setFrameDimen( new Vector2(500, 500));
		txLevel1Background1.setFrameDimen( new Vector2(1500, 500));
		txLevel1Background2.setFrameDimen( new Vector2(1500, 500));
		txLevel1Background3.setFrameDimen( new Vector2(1500, 500));
		txLevel1Foreground.setFrameDimen(  new Vector2(1500, 500));

		txLevel2Sky.setFrameDimen( new Vector2(500, 500));
		txLevel2Background1.setFrameDimen( new Vector2(1500, 500));
		txLevel2Background2.setFrameDimen( new Vector2(1500, 500));
		txLevel2Background3.setFrameDimen( new Vector2(1500, 500));
		txLevel2Foreground.setFrameDimen( new Vector2(1500, 500) );
	}
}




