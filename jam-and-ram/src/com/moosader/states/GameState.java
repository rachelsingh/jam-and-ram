// Pickin' Sticks Arcade, Rachel J. Morris 2012
// https://github.com/Moosader/Pickin-Sticks-Arcade
// www.moosader.com
// Licensed TBD!

package com.moosader.states;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.moosader.entities.Level;
import com.moosader.jamram.GlobalOptions;
import com.moosader.managers.GameStateEntityManager;
import com.moosader.screens.GameScreen;

// This is the game state for Pickin' Sticks Classic--
// The really simple old game where all you do is pick up sticks endlessly
public class GameState extends BaseState {	
	private OrthographicCamera m_camera;
	private SpriteBatch m_batch;
	private GameScreen m_screen;
	
	private boolean m_isDone;
	private GameStateEntityManager m_entityManager;
	private Level m_level;
	
	//private BitmapFont m_font;
	//private CharSequence m_playerHealthLabel;
	//private CharSequence m_bossHealthLabel;
	
	private int m_levelId;
	
	public GameState() {
		
	}
	
	public GameState(int level, boolean humanPilot, boolean humanGunner) { setup(level, humanPilot, humanGunner); }
	
	public void reset() {
		setup(m_levelId, m_entityManager.isPilotHuman(), m_entityManager.isGunnerHuman());
	}
	
	public void setup(int level, boolean humanPilot, boolean humanGunner) {
		m_levelId = level;
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		
		m_screen = new GameScreen();
		m_screen.setup();

		m_isDone = false;
		m_camera = new OrthographicCamera();
		m_camera.setToOrtho(false, w, h);
		m_batch = new SpriteBatch();
		
		m_entityManager = new GameStateEntityManager();
		m_entityManager.setup(level);
		m_entityManager.setGunnerHuman(humanGunner);
		m_entityManager.setPilotHuman(humanPilot);
		
		m_level = new Level();
		m_level.setup(level);
		
		/*
		m_font = new BitmapFont(Gdx.files.internal("data/fonts/Averia.fnt"),
				Gdx.files.internal("data/fonts/Averia.png"), false);
		m_playerHealthLabel = "JAM&RAM:";
		m_bossHealthLabel = "JAD:";
		m_font.setColor(1.0f, 1.0f, 1.0f, 1.0f);
		*/
	}
	
	public String getMusicPath() {
		if ( m_levelId == 1 ) {
			if ( GlobalOptions.isHalloween() ) {
				return "data/halloween/audio/MoonlightHall_KevinMacLeod.mp3";			
			}
			else {
				return "data/audio/MistakeTheGetaway_KevinMacLeod.mp3";	
			}
		}
		else {
			if ( GlobalOptions.isHalloween() ) {
				return "data/halloween/audio/ConstancyPartThree_KevinMacLeod.mp3";			
			}
			else {
				return "data/audio/Mechanolith_KevinMacLeod.mp3";			
			}	
		}
	}
	
	public void dispose() {
		m_batch.dispose();
	}
	
	@Override
	public void resize(int width, int height){
		// Update the screen positioning!
		//System.out.println( "Resize!" );
		//m_gameScreen.resize();
	}

	@Override
	public void update(float delta) {
		m_entityManager.update(delta);
		m_level.update(delta);
		
		if (Gdx.input.isKeyPressed(Keys.ESCAPE)) {
			m_isDone = true;
		}

		m_level.addBackgroundSprites(m_screen);
		m_entityManager.addEntitiesToSpriteList(m_screen);
		m_level.addForegroundSprites(m_screen);
		
		// Populate sprite list
		m_camera.update();
		
		if ( m_entityManager.gameOver() ) {
			m_isDone = true;
			m_nextState = NextState.MENU_SCREEN;
			
		}
		else if ( m_entityManager.nextLevel() ) {
			m_isDone = true;
			if ( m_levelId == 1 ) {
				m_nextState = NextState.LEVEL2;
			}
			else {
				m_nextState = NextState.YOU_WIN;
			}
		}
	}
	
	@Override
	public void draw(float delta) {		
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		m_batch.setProjectionMatrix(m_camera.combined);
		m_batch.begin();
			m_screen.drawScaledGraphics(m_batch);
		m_batch.end();	
	}

	@Override
	public boolean isDone() {
		return m_isDone;
	}
}
