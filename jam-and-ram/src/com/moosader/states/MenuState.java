// Pickin' Sticks Arcade, Rachel J. Morris 2012
// https://github.com/Moosader/Pickin-Sticks-Arcade
// www.moosader.com
// Licensed TBD!

package com.moosader.states;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.moosader.jamram.GlobalOptions;
import com.moosader.managers.GraphicsManager;
import com.moosader.managers.MenuStateGuiManager;

public class MenuState extends BaseState {
	private SpriteBatch m_batch;
	private OrthographicCamera m_camera;
	private ArrayList<Sprite> m_lstSprites;
	
	private MenuStateGuiManager m_guiManager;
	
	private Sprite m_sprIntro;
	
	private boolean m_humanPilot;
	private boolean m_humanGunner;
	private boolean m_introVisible;
	
	public MenuState() { setup(); }
	
	public void reset() {
		setup();
	}

	public void dispose() {
	}
	
	public void setup() {
		m_introVisible = false;
		m_nextState = NextState.LEVEL1;
		m_camera = new OrthographicCamera();
		m_camera.setToOrtho(true, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		
		m_isDone = false;
		m_lstSprites = new ArrayList<Sprite>();
		m_batch = new SpriteBatch();
		
		m_guiManager = new MenuStateGuiManager();
		m_guiManager.setup();
		
		m_humanPilot = true;
		m_humanGunner = true;

		m_sprIntro = new Sprite(new TextureRegion(GraphicsManager.txIntroSlide.texture, 
				0, 0, GraphicsManager.txIntroSlide.frameW(), GraphicsManager.txIntroSlide.frameH()));
		m_sprIntro.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
	}
	
	public String getMusicPath() {
		if ( GlobalOptions.isHalloween() ) {
			return "data/halloween/audio/LiftMotif_KevinMacLeod.mp3";			
		}
		else {
			return "data/audio/JustNasty_KevinMacLeod.mp3";			
		}
	}
	
	public boolean isPilotHuman() {
		return m_humanPilot;
	}
	
	public boolean isGunnerHuman() {
		return m_humanGunner;
	}

	@Override
	public void update(float delta) {
		
		if ( m_introVisible == false ) {
			if (Gdx.input.isKeyPressed(Keys.F1) || Gdx.input.isKeyPressed(Keys.Z)) {
				m_humanPilot = false;
				m_humanGunner = true;
				m_introVisible = true;
			}
			else if (Gdx.input.isKeyPressed(Keys.F2) || Gdx.input.isKeyPressed(Keys.X)) {
				m_humanPilot = true;
				m_humanGunner = true;
				m_introVisible = true;				
			}
			m_lstSprites.clear();
			m_guiManager.addElementsToSpriteList(m_lstSprites);
		}
		else {
			if (!Gdx.input.isKeyPressed(Keys.F1) && !Gdx.input.isKeyPressed(Keys.F2) &&
					!Gdx.input.isKeyPressed(Keys.Z) && !Gdx.input.isKeyPressed(Keys.X) && Gdx.input.isKeyPressed(Keys.ANY_KEY)) {
				m_isDone = true;
			}
			m_lstSprites.clear();
			m_lstSprites.add(m_sprIntro);
		}
		
		// Handle menu buttons
		/*
		if (Gdx.input.isTouched()) {
			float x = Gdx.input.getX();
			float y = Gdx.input.getY();
			
			float w = Gdx.graphics.getWidth();
			float h = Gdx.graphics.getHeight();
			
			float buttonX = 0.711f * w;
			float buttonY = 0.882f * h;
			float buttonW = 0.274f * w;
			float buttonH = 0.099f * h;
			
			if ( x > buttonX && x < (buttonX + buttonW) &&
					y > buttonY && y < (buttonY + buttonH) ) {
				m_isDone = true;
			}
			
		}
		 */
	}
	
	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void draw(float delta) {		
		Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		m_batch.begin();
		for (Sprite s : m_lstSprites)
		{
			s.draw(m_batch);
		}
		m_guiManager.drawText(m_batch);
		m_batch.end();	
	}

}
