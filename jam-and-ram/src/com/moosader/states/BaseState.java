// Pickin' Sticks Arcade, Rachel J. Morris 2012
// https://github.com/Moosader/Pickin-Sticks-Arcade
// www.moosader.com
// Licensed TBD!

package com.moosader.states;

import com.badlogic.gdx.Screen;

//
public abstract class BaseState implements Screen	 {
	public enum NextState {
		TITLE_SCREEN, MENU_SCREEN, INTRO, LEVEL1, LEVEL2, YOU_WIN
	}
	public NextState m_nextState;	
		
	public abstract void update(float delta);
	public abstract void draw(float delta);
	
	public boolean m_isDone;
	
	public boolean isDone() { return m_isDone; }
	
	@Override
	public void render(float delta){
		update(delta);
		draw(delta);
	}
	
	public String getMusicPath() {
		return "";
	}
	
	public abstract void reset();
	
	@Override
	public abstract void resize(int width, int height);
	
	@Override
	public void show() {
	}
	
	@Override
	public void hide() {
	}
	
	@Override
	public void pause() {
	}
	
	@Override
	public void resume() {
	}
	
	@Override
	public void dispose() {
	}
}
