package com.moosader.jamram;

public class GlobalOptions {
	// Loads halloween assets
	public static boolean halloweenVersion = false;
	// Loads lower-resolution graphics for 320x480 screen
	public static boolean androidVersion = false;
	
	public static void setIsHalloween(boolean val) {
		halloweenVersion = val;
	}
	
	public static void setIsAndroid(boolean val) {
		androidVersion = val;
	}
	
	public static boolean isHalloween() {
		return halloweenVersion;
	}
	
	public static boolean isAndroid() {
		return androidVersion;
	}
}
