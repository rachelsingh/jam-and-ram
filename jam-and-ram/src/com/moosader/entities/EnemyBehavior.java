package com.moosader.entities;

public enum EnemyBehavior {
	FORWARD, FOLLOW_PLAYER
}
