package com.moosader.entities;

public enum BulletState {
	INACTIVE, CHARGING, PROJECTILE
}
