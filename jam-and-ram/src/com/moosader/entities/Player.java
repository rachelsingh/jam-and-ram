// Pickin' Sticks Arcade, Rachel J. Morris 2012
// https://github.com/Moosader/Pickin-Sticks-Arcade
// www.moosader.com
// Licensed TBD!

package com.moosader.entities;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.moosader.jamram.GlobalOptions;
import com.moosader.managers.GraphicsManager;

// Polling: http://code.google.com/p/libgdx/wiki/InputPolling
// Event: http://code.google.com/p/libgdx/wiki/InputEvent

public class Player extends Character {
	protected final float m_liftSpeed = 10f;
	protected final float m_gravitySpeed = 5f;
	// Vector2 m_speed
	
	protected float m_verticalAcceleration;
	protected final float m_maxAcceleration = 4.0f;
	
	public enum PilotState { PLAYER, AI };
	public enum GunnerState { PLAYER, AI };
	public PilotState m_pilotControl;
	public GunnerState m_gunnerControl;
	
	protected boolean m_buttonLiftPressed;
	
	// For AI movement
	protected Random m_moveChooser;
	protected int m_choiceCounter;
	protected int m_moveDir;
	protected int m_upMoves;
	protected int m_downMoves;
	
	protected final float m_regenRate = 1.0f; // For AI pilot only!
	
	public void setup() {
		super.setup();
		m_hp = m_totalHP = 70 * 10; // 10 shots "lives"
		m_coord = new Vector2(16f, 500f);
		m_dimen = new Vector2(200f, 150f);
		m_pilotControl = PilotState.PLAYER;
		m_gunnerControl = GunnerState.PLAYER;
		m_buttonLiftPressed = false;
		m_verticalAcceleration = 0f;
		m_shootCooldown = 0f;
		m_shootCooldownMax = 20.0f;
		setupGraphics();
		m_chargingBullet.setupPlayerBullet(m_bulletSprite);
		m_moveChooser = new Random();
		m_choiceCounter = 0;
		m_moveDir = 0;
		m_upMoves = m_downMoves = 0;		
		updateFrame();
	}
	
	protected void setupGraphics() {		
		m_region = new TextureRegion(GraphicsManager.txPlayer.texture, 0, 0, 
				(int)GraphicsManager.txPlayer.frameW(), (int)GraphicsManager.txPlayer.frameH());
		m_sprite = new Sprite(m_region);
		
		m_bulletSprite = new Sprite(new TextureRegion(GraphicsManager.txPlayerBullet.texture, 0, 0,
				GraphicsManager.txPlayerBullet.frameW(), GraphicsManager.txPlayerBullet.frameH()));
		
		Sprite heartSprite = new Sprite(new TextureRegion(GraphicsManager.txHudHP.texture, 0, 0, 
				GraphicsManager.txHudHP.frameW(), GraphicsManager.txHudHP.frameH()));
		heartSprite.setPosition(0, 0);
		heartSprite.setSize(32, 32);

		m_lstHPSprites.add(new Sprite(heartSprite));
		m_lstHPSprites.add(new Sprite(heartSprite));
		m_lstHPSprites.add(new Sprite(heartSprite));
		m_lstHPSprites.add(new Sprite(heartSprite));
		m_lstHPSprites.add(new Sprite(heartSprite));
	}
	
	protected void handleGun() {
		if ( m_gunnerControl == GunnerState.PLAYER ) {
			if (Gdx.input.isKeyPressed(Keys.L)){
				m_chargingBullet.shootingPressed();
				m_chargingBullet.charge(m_coord, m_dimen);
			}
			else {
				// Either turns bullet into projectile, or inactive
				// based on whether the key was being held.
				m_chargingBullet.shootingReleased();
			}			
		}
		else {
			
		}
	}
	
	public void handleMovement(Boss boss) {
		if ( m_pilotControl == PilotState.PLAYER ) {
			if (Gdx.input.isKeyPressed(Keys.A)){
				m_buttonLiftPressed = true;
			}
			else {
				m_buttonLiftPressed = false;
			}
		}
		else {
			m_choiceCounter++;
			
			if (getBottom() + m_dimen.y/2 < boss.getTop() ) {
				m_moveDir = 0;
			}
			else if (getBottom() + m_dimen.y/2 > boss.getBottom() ) {
				m_moveDir = 1;				
			}
			
			if ( m_moveDir == 0 ) {
				m_buttonLiftPressed = true;
				m_upMoves++;
				
			}
			else if ( m_moveDir == 1 ) {
				m_buttonLiftPressed = false;
				m_downMoves++;
			}			
		}		
	}
	
	public void move(float delta) {
		if ( m_buttonLiftPressed ) {
			// Rise
			m_verticalAcceleration += m_liftSpeed * delta;
		}
		else {
			// Fall
			m_verticalAcceleration -= m_gravitySpeed * delta;
		}
		checkBounds();
		
		// Move player
		m_coord.y += m_verticalAcceleration;
		
		if (m_pilotControl == PilotState.AI && m_hp < m_totalHP/2) {
			m_hp += m_regenRate;
		}
		
		// Move hearts
		int hearts = m_hp / (m_totalHP/5);
		// Sanity check
		if ( hearts > m_lstHPSprites.size() ) {
			hearts = m_lstHPSprites.size();
		}
		for ( int index = hearts; index < m_lstHPSprites.size(); index++ ) {
			m_lstHPSprites.get(index).setPosition(m_lstHPSprites.get(index).getX(), m_lstHPSprites.get(index).getY() + 10);
		}
		for ( int index = 0; index < hearts; index++ ) {
			m_lstHPSprites.get(index).setPosition(m_coord.x + (index * 32), m_coord.y + m_dimen.y);
		}
	}
	
	private void checkBounds() {
		// Acceleration bounds
		if (m_verticalAcceleration > m_maxAcceleration) {
			m_verticalAcceleration = m_maxAcceleration;
		}
		else if (m_verticalAcceleration < -m_maxAcceleration) {
			m_verticalAcceleration = -m_maxAcceleration;
		}
		
		// Position bounds
		if (m_pilotControl == PilotState.PLAYER) {
			if (m_coord.y > 1000 - m_dimen.y) {
				// Off the top
				m_verticalAcceleration = 0f;
				m_coord.y = 1000 - m_dimen.y;
			}
			else if (m_coord.y < 0) {
				// Off the bottom
				m_verticalAcceleration = 0f;
				m_coord.y = 0f;
			}
		}
		else {
			// Position bounds
			if (m_coord.y > 1000 - m_dimen.y - 100) {
				// Off the top
				m_coord.y = 1000 - m_dimen.y - 100;
			}
			else if (m_coord.y < 0 + 100) {
				// Off the bottom
				m_coord.y = 100;
			}		
		}
	}

	public boolean crash() {
		if ( m_coord.y > -m_dimen.y*3 ) {
			m_coord.y -= m_gravitySpeed;
			m_coord.x += 5f;
			updateFrame();
			return true;
		}
		return false;
	}
	
	protected void incrementFrame() {
		// TODO : Have halloween graphics have all the same animations
		if ( GlobalOptions.isHalloween() ) {
			m_frame = 0;
		}
		else {
			m_frame += 0.1f;
			if ( m_frame > 3f ) {
				m_frame = 0f;
			}
		}
		
		int frame = (int)(m_frame);
		
		m_region.setRegion(
				(int)(frame*GraphicsManager.txPlayer.frameW()), 0,
				(int)(GraphicsManager.txPlayer.frameW()), 
				(int)(GraphicsManager.txPlayer.frameH()));
	}
}
