package com.moosader.entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.moosader.managers.GraphicsManager;
import com.moosader.screens.GameScreen;

public class Explosion extends Effect {
	/*
	protected float m_animateSpeed;
	protected Vector2 m_frameDimen;
	protected int m_totalFrames;
	protected float m_frame;
	protected float m_size;
	protected boolean m_active;
	 */
	public Explosion(EntityBase object) {
		setup(object);
		setup();
	}
	
	public Explosion(EntityBase object, float size) {
		setup(object);
		setup();
		setSize(size);
	}
	
	public void setup(EntityBase object) {
		m_coord = new Vector2();
		m_size = 64f;
		m_dimen = new Vector2(m_size, m_size);
		m_coord.x = object.getLeft() + object.getWidth() / 2 - m_size / 2;
		m_coord.y = object.getBottom() + object.getHeight() / 2 - m_size / 2;
		
	}
	
	public void setup() {
		m_region = new TextureRegion(GraphicsManager.txEffectExplosion.texture, 
				0, 0, (int)(GraphicsManager.txEffectExplosion.frameW()), (int)(GraphicsManager.txEffectExplosion.frameH()));
		m_sprite = new Sprite(m_region);
		m_active = true;
		m_totalFrames = 4;
		updateFrame();
	}
	
	public void setCoordinates(Vector2 coords) {
		m_coord = coords;
	}
	
	public void setSize(float size) {
		m_size = size;
		m_dimen.x = m_dimen.y = size;
	}
	
	public void update(float delta) {
		updateFrame();
	}
	
	public boolean isActive() {
		return m_active;
	}
	
	protected void incrementFrame() {
		m_frame += 0.1f;
		if (m_frame > m_totalFrames) {
			m_active = false;
		}
		m_region.setRegion(
				(int)((int)(m_frame)*GraphicsManager.txEffectExplosion.frameW()), 0,
				(int)(GraphicsManager.txEffectExplosion.frameW()), (int)(GraphicsManager.txEffectExplosion.frameH()));
	}
	
	protected void updateFrame() {
		incrementFrame();
		m_sprite.setRegion(m_region);
		m_sprite.setPosition(m_coord.x, m_coord.y);
		m_sprite.setSize(m_dimen.x, m_dimen.y);
	}
	
	public void addToSpriteList(GameScreen screen) {
		screen.add(m_sprite);
	}
}
