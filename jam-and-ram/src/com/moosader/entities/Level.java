package com.moosader.entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.moosader.managers.GraphicsManager;
import com.moosader.screens.GameScreen;

public class Level {
	private Sprite m_sprSky;
	private Sprite m_sprBuildings1;
	private Sprite m_sprBuildings2;
	private Sprite m_sprBuildings3;
	private Sprite m_sprForeground;
	private float m_speed;
	private float m_maxSpeed;
		
	public Level() {
	}
	
	public void setup(int level) {				
		m_speed = 1.0f;
		m_maxSpeed = 5.0f;
		
		if ( level == 1 ) { 		
			m_sprSky = new Sprite(new TextureRegion(GraphicsManager.txLevel1Sky.texture, 
					0, 0, GraphicsManager.txLevel1Sky.frameW(), GraphicsManager.txLevel1Sky.frameH()));
			
			m_sprBuildings1 = new Sprite(new TextureRegion(GraphicsManager.txLevel1Background1.texture, 
					0, 0, GraphicsManager.txLevel1Background1.frameW(), GraphicsManager.txLevel1Background1.frameH()));
			
			m_sprBuildings2 = new Sprite(new TextureRegion(GraphicsManager.txLevel1Background2.texture, 
					0, 0, GraphicsManager.txLevel1Background2.frameW(), GraphicsManager.txLevel1Background2.frameH()));
			
			m_sprBuildings3 = new Sprite(new TextureRegion(GraphicsManager.txLevel1Background3.texture, 
					0, 0, GraphicsManager.txLevel1Background3.frameW(), GraphicsManager.txLevel1Background3.frameH()));
			
			m_sprForeground = new Sprite(new TextureRegion(GraphicsManager.txLevel1Foreground.texture, 
					0, 0, GraphicsManager.txLevel1Foreground.frameW(), GraphicsManager.txLevel1Foreground.frameH()));
		}
		else if ( level == 2 ) {		
			m_sprSky = new Sprite(new TextureRegion(GraphicsManager.txLevel2Sky.texture, 
					0, 0, GraphicsManager.txLevel2Sky.frameW(), GraphicsManager.txLevel2Sky.frameH()));
			
			m_sprBuildings1 = new Sprite(new TextureRegion(GraphicsManager.txLevel2Background1.texture, 
					0, 0, GraphicsManager.txLevel2Background1.frameW(), GraphicsManager.txLevel2Background1.frameH()));
			
			m_sprBuildings2 = new Sprite(new TextureRegion(GraphicsManager.txLevel2Background2.texture, 
					0, 0, GraphicsManager.txLevel2Background2.frameW(), GraphicsManager.txLevel2Background2.frameH()));
			
			m_sprBuildings3 = new Sprite(new TextureRegion(GraphicsManager.txLevel2Background3.texture, 
					0, 0, GraphicsManager.txLevel2Background3.frameW(), GraphicsManager.txLevel2Background3.frameH()));
			
			m_sprForeground = new Sprite(new TextureRegion(GraphicsManager.txLevel1Foreground.texture, 
					0, 0, GraphicsManager.txLevel2Foreground.frameW(), GraphicsManager.txLevel2Foreground.frameH()));		
		}
		
		m_sprSky.setSize(1000, 1000);
		m_sprBuildings1.setSize(3000, 1000);
		m_sprBuildings2.setSize(3000, 1000);
		m_sprBuildings3.setSize(3000, 1000);		
		m_sprForeground.setSize(3000, 1000);	
	}
	
	public void update(float delta) {
		m_speed += 0.01f;
		if ( m_speed > m_maxSpeed ) {
			m_speed = m_maxSpeed;
		}
		updateBackgroundOffsets();
	}
	
	private void updateBackgroundOffsets() {
		// TODO: Change to regions
		m_sprBuildings1.setX( m_sprBuildings1.getX() - (m_speed*0.5f) );
		m_sprBuildings2.setX( m_sprBuildings2.getX() - (m_speed*1.0f) );
		m_sprBuildings3.setX( m_sprBuildings3.getX() - (m_speed*2.0f) );
		//m_sprClouds1.setX( m_sprClouds1.getX() - (m_speed*2.1f) );
		//m_sprClouds2.setX( m_sprClouds1.getX() + 3000 );
		m_sprForeground.setX( m_sprForeground.getX() - (m_speed*3.0f) );
		
		if ( m_sprBuildings1.getX() < -3000 ) {
			m_sprBuildings1.setX(1000);
		}
		if ( m_sprBuildings2.getX() < -3000 ) {
			m_sprBuildings2.setX(1000);
		}
		if ( m_sprBuildings3.getX() < -3000 ) {
			m_sprBuildings3.setX(1000);
		}
		if ( m_sprForeground.getX() < -3000 ) {
			m_sprForeground.setX(1000);
		}
		/*
		if ( m_sprClouds1.getX() <= -3000 ) {
			m_sprClouds1.setX(0);
		}
		*/
	}

	public void addBackgroundSprites(GameScreen screen) {
		screen.add(m_sprSky);
		screen.add(m_sprBuildings1);
		screen.add(m_sprBuildings2);
		screen.add(m_sprBuildings3);
		//screen.add(m_sprClouds1);	
		//screen.add(m_sprClouds2);		
	}
	
	public void addForegroundSprites(GameScreen screen) {
		screen.add(m_sprForeground);			
	}
}
