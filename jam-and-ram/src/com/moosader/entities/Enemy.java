package com.moosader.entities;

import java.util.Random;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.moosader.jamram.GlobalOptions;
import com.moosader.managers.GraphicsManager;
import com.moosader.managers.GraphicsManager.TextureWrapper;

public class Enemy extends Character {
	protected EnemyBehavior m_behavior;
	protected EnemyType m_type;
	protected EntityBase m_target;
	protected TextureWrapper m_enemyTW;
	
	EnemyType m_enemyType;
	
	public void setup(int type, int behavior, Vector2 randCoord) {
		super.setup();
		m_shootCooldown = 0f;
		m_hp = 100;
		m_type = EnemyType.values()[type];
		if (m_type == EnemyType.SHOTGUNAGENT1 || m_type == EnemyType.SHOTGUNAGENT2) {
			// Parachute guys can only go downwards
			m_behavior = EnemyBehavior.FORWARD;
		}
		else {
			m_behavior = EnemyBehavior.values()[behavior];			
		}
		setupEnemy();
		m_chargingBullet.setupEnemyBullet(m_bulletSprite, m_speed.x);
		m_coord = new Vector2(randCoord);
		m_shootCooldownMax = 100.0f;
		
		updateFrame();
	}
	
	protected void setupEnemy() {
		Vector2 speed = new Vector2();
		
		Random rand = new Random();
				
		if (m_type == EnemyType.DUALAGENTS1) {
			m_enemyTW = GraphicsManager.txEnemy1;
			speed.x = -(rand.nextInt( 4 ) + 1 ) * 50;
			speed.y = (rand.nextInt(3) - 1) * 10;
		}
		else if (m_type == EnemyType.DUALAGENTS2) {
			m_enemyTW = GraphicsManager.txEnemy2;
			speed.x = -(rand.nextInt( 4 ) + 1 ) * 50;
			speed.y = (rand.nextInt(3) - 1) * 10;
		}
		else if (m_type == EnemyType.PISTOLAGENT1) {
			m_enemyTW = GraphicsManager.txEnemy3;
			speed.x = -(rand.nextInt( 5 ) + 3) * 50;
			speed.y = (rand.nextInt(3) - 1) * 20;
		}
		else if (m_type == EnemyType.PISTOLAGENT2) {
			m_enemyTW = GraphicsManager.txEnemy4;
			speed.x = -(rand.nextInt( 5 ) + 3) * 50;
			speed.y = (rand.nextInt(3) - 1) * 20;
		}
		else if (m_type == EnemyType.SHOTGUNAGENT1) {	
			m_enemyTW = GraphicsManager.txEnemy5;
			speed.x = -(rand.nextInt( 5 ) + 1) * 50;
			speed.y = -75f;
		}
		else { // (m_type == EnemyType.SHOTGUNAGENT2) {
			m_enemyTW = GraphicsManager.txEnemy6;
			speed.x = -(rand.nextInt( 5 ) + 1) * 50;
			speed.y = -75f;
		}
		
		m_speed = new Vector2(speed);
		m_dimen = new Vector2(m_enemyTW.frameW() / 4, m_enemyTW.frameH() / 4);
		
		m_region = new TextureRegion(m_enemyTW.texture, 0, 0, (int)m_enemyTW.frameW(), (int)m_enemyTW.frameH());
		m_sprite = new Sprite(m_region);
		
		m_bulletSprite = new Sprite(new TextureRegion(GraphicsManager.txEnemyBullet.texture, 0, 0, GraphicsManager.txEnemyBullet.frameW(), GraphicsManager.txEnemyBullet.frameH()));
	}
	
	public void setTarget(EntityBase target) {
		m_target = target;
	}
	
	protected void handleGun() {
		// Decide whether to shoot
		if (m_target.getTop() > getBottom() &&
				m_target.getBottom() < getTop()) {
			// Shoot!
			m_chargingBullet.alignWithOwner(m_coord, m_dimen);
			m_chargingBullet.setProjectile();
		}
	}
	
	public void move(float delta) {
		m_coord.x += m_speed.x * delta;

		// Decide how to move vertically
		if (m_behavior == EnemyBehavior.FORWARD) {
			m_coord.y += m_speed.y * delta;
		}
		else if (m_behavior == EnemyBehavior.FOLLOW_PLAYER) {
			if (m_target.getTop() < getBottom()) {
				m_coord.y -= m_speed.y * delta;
			}
			else if (m_target.getBottom() > getTop()) {
				m_coord.y += m_speed.y * delta;
			}
		}
	}
	
	public void handleMovement() {
		// This should be where the AI would decide
		// to move, if it weren't just one of two options.
		// Also, this will go away when I have actual maps
	}
	
	protected void incrementFrame() {
		// TODO: Halloween cleanup
		if ( GlobalOptions.isHalloween() ) {
			m_frame = 0;
		}
		else {
			m_frame += 0.1f;
			if ( m_frame > 3f ) {
				m_frame = 0f;
			}
		}
		
		// TODO: Animation fix
		int frame = (int)(m_frame);
		if ( frame == 3 ) {
			frame = 2;
		}
		
		m_region.setRegion(
				(int)(frame*m_enemyTW.frameW()), 0,
				(int)(m_enemyTW.frameW()), (int)(m_enemyTW.frameH()));
	}
}
