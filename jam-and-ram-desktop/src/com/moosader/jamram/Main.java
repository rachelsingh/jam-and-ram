package com.moosader.jamram;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration(); 
		
		if ( GlobalOptions.isHalloween() ) {
			cfg.title = "Halloween Flight";
		}
		else {
			cfg.title = "JAM&RAM";			
		}
		
		cfg.useGL20 = false;
		cfg.width = 600; 
		cfg.height = 600;
		
		new LwjglApplication(new JamAndRam(), cfg);
	}
}
 